sigal-admin
===========

Warning
--------
This is a very recent project. It means that everything is working fine for me, but there is no unit test (yet?), so please experiment it on fake or backed up data first.

It shouldn't destroy your hard disk though.

![sigal_admin](./doc/screenshots/alpha.png)

The goal of this project is to provide a friendly user interface for editing
metadata before generating a gallery with [sigal](http://sigal.saimon.org/).

Install
-------

    git clone https://framagit.org/Glandos/sigal-admin.git
    
Run
---
First, edit `SELECTED_MEDIA_DIRECTORY` in `sigal-admin/sigal-admin.py` to set it to your current media directory. Then, launch the following:

    export FLASK_APP=sigal-admin/sigal-admin.py
    flask run
    
And you should be able to see something on [http://127.0.0.1:5000/](http://127.0.0.1:5000/).

Use
---
1. Select the title field and type your media title
2. Hit tab or select the description field and type your media description
3. Hit tab to go to the next title field
4. Repeat from step 2.

Each time the focus goes out from the media line, metadata are saved on disk. There is no visual indicator for this right now.

After editing, run your usual `sigal build` command to enjoy more metadata.

Future plans
------------
A lot of things are missing

- A better UI
  - Better CSS
  - Indicator for save operations
- More supported field
- Ability to edit media filter, to exclude media from sigal generated gallery
- < Your idea here >