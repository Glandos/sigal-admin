#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  4 20:48:34 2018

@author: adrien
"""

import base64

from pathlib import Path
from collections import defaultdict

from flask import Flask, render_template, send_from_directory, request, \
                    jsonify, redirect, url_for
from .sigalcompat import parse_metadata

try:
    import exifread
except ModuleNotFoundError:
    class exifread:
        pass
    exifread.process_file = lambda _: {}


SELECTED_MEDIA_DIRECTORY = Path('/path/to/selected/photos/')
VIDEO_EXTENSIONS = ['.mov', '.avi', '.mp4', '.webm', '.ogv', '.3gp']
IMAGE_EXTENSIONS = ['.jpg', '.jpeg', '.png', '.gif']
MEDIA_EXTENSIONS = VIDEO_EXTENSIONS + IMAGE_EXTENSIONS


app = Flask(__name__)


def read_metadata(media_path: Path):
    metadata_path = media_path.with_suffix('.md')
    metadata = defaultdict(str)
    if metadata_path.exists():
        parsed_metadata, description = parse_metadata(metadata_path)
        metadata.update({name: data[0]
                         for (name, data) in parsed_metadata.items()})
        metadata['description'] = description
    return metadata


def write_metadata(media_path: Path, metadata: dict):
    metadata_path = media_path.with_suffix('.md')
    with open(metadata_path, 'w') as metadata_file:
        description = metadata.pop('description')
        metadata_file.writelines([f'{name.capitalize()}: {value}\n'
                                  for (name, value) in metadata.items()])
        # Add empty line between tags and description
        metadata_file.writelines(['\n', description])


def read_exif(media_path: Path):
    tags = {}
    with open(media_path, 'rb') as opened_media:
        tags = exifread.process_file(opened_media)
    # Simplification. Get values[0] if it's the only value
    return {name: t if not hasattr(t, 'values')
                  else t.values if len(t.values) > 1
                  else t.values[0]
            for name, t in tags.items()}


def get_thumbnail_url(media_path: Path, exif: dict):
    if 'JPEGThumbnail' in exif:
        encoded_image = base64.b64encode(exif['JPEGThumbnail']).decode('ascii')
        return f'data:image/jpeg;base64,{encoded_image}'
    else:
        return f'/media/{media_path.relative_to(SELECTED_MEDIA_DIRECTORY)}'


@app.route('/media/<path:media>')
def serve_media(media):
    return send_from_directory(SELECTED_MEDIA_DIRECTORY, media)


@app.route('/edit/<path:subdirectory>', methods=['POST'])
def update_metadata(subdirectory):
    media_path = SELECTED_MEDIA_DIRECTORY / subdirectory / request.json['name']
    write_metadata(media_path, request.json['metadata'])
    return jsonify(ok=True)


@app.route('/edit/<path:subdirectory>/')
@app.route('/edit/')
def edit(subdirectory='.'):
    current_directory = SELECTED_MEDIA_DIRECTORY / subdirectory
    subdirectories = []
    medias = []
    for child in current_directory.iterdir():
        if child.is_dir():
            subdirectories.append(child.relative_to(SELECTED_MEDIA_DIRECTORY))
        elif child.is_file() and child.suffix.lower() in MEDIA_EXTENSIONS:
            exif = read_exif(child)
            media = {
                'image_url': get_thumbnail_url(child, exif),
                'name': child.name,
                'path': subdirectory,
                'metadata': read_metadata(child),
                'exif': exif,
                }
            medias.append(media)

    medias.sort(key=lambda m: m['name'])
    return render_template('index.html',
                           subdirectories=subdirectories,
                           medias=medias)


@app.route('/')
def main():
    return redirect(url_for('edit'))
