import codecs
import re

from markdown import Markdown
from markupsafe import Markup


def read_markdown(filename):
    """Reads markdown file, converts output and fetches title and meta-data for
    further processing.
    """
    # Use utf-8-sig codec to remove BOM if it is present. This is only possible
    # this way prior to feeding the text to the markdown parser (which would
    # also default to pure utf-8)
    with codecs.open(filename, 'r', 'utf-8-sig') as f:
        text = f.read()

    md = Markdown(extensions=['markdown.extensions.meta',
                              'markdown.extensions.tables'],
                  output_format='html5')
    # Mark HTML with Markup to prevent jinja2 autoescaping
    output = {'description': Markup(md.convert(text))}

    try:
        meta = md.Meta.copy()
    except AttributeError:
        pass
    else:
        output['meta'] = meta
        try:
            output['title'] = md.Meta['title'][0]
        except KeyError:
            pass

    return output


# Copied from https://github.com/Python-Markdown/markdown/blob/b62ddeda02fadcd09def9354eb2ef46a7562a106/markdown/extensions/meta.py

# Global Vars
META_RE = re.compile(r'^[ ]{0,3}(?P<key>[A-Za-z0-9_-]+):\s*(?P<value>.*)')
META_MORE_RE = re.compile(r'^[ ]{4,}(?P<value>.*)')
BEGIN_RE = re.compile(r'^-{3}(\s.*)?')
END_RE = re.compile(r'^(-{3}|\.{3})(\s.*)?')


def parse_metadata(filename):
    """ Parse Meta-Data.
    """
    meta = {}
    key = None
    # The original function was feeded with lines directly
    lines = []
    with codecs.open(filename, 'r', 'utf-8-sig') as f:
        lines = f.readlines()

    if lines and BEGIN_RE.match(lines[0]):
        lines.pop(0)
    while lines:
        line = lines.pop(0)
        m1 = META_RE.match(line)
        if line.strip() == '' or END_RE.match(line):
            break  # blank line or end of YAML header - done
        if m1:
            key = m1.group('key').lower().strip()
            value = m1.group('value').strip()
            try:
                meta[key].append(value)
            except KeyError:
                meta[key] = [value]
        else:
            m2 = META_MORE_RE.match(line)
            if m2 and key:
                # Add another line to existing key
                meta[key].append(m2.group('value').strip())
            else:
                lines.insert(0, line)
                break  # no meta data - done
    return (meta, '\n'.join(lines))
