;
'use strict';

function postMetadata(url, metadata) {
    // Create new promise with the Promise() constructor;
    // This has as its argument a function
    // with two parameters, resolve and reject
    return new Promise(function(resolve, reject) {
      // Standard XHR to load an image
      var request = new XMLHttpRequest();
      request.open('POST', url);
      request.setRequestHeader('Content-Type', 'application/json')
      request.responseType = 'json';
      // When the request loads, check whether it was successful
      request.onload = function() {
        if (request.status === 200) {
        // If successful, resolve the promise by passing back the request response
          resolve(request.response);
        } else {
        // If it fails, reject the promise with a error message
          reject(Error('Metadata have not been successfully stored; error code:' + request.statusText));
        }
      };
      request.onerror = function() {
      // Also deal with the case when the entire request fails to begin with
      // This is probably a network error, so reject the promise with an appropriate message
          reject(Error('There was a network error.'));
      };
      // Send the request
      request.send(metadata);
    });
}

function onBlurredMedia(event) {
    let currentContainer = event.currentTarget;
    if(currentContainer.contains(event.relatedTarget)
        || 'modified' in currentContainer.dataset === false) {
        return;
    }
    let media_name = currentContainer.dataset.media_name;
    let media_path = currentContainer.dataset.media_path;
    let title = currentContainer.querySelector('input[name="title"]').value;
    let description = currentContainer.querySelector('textarea[name="description"]').value;
    let update_request = {
        path: currentContainer.dataset.media_path,
        name: currentContainer.dataset.media_name,
        metadata: {
            title: currentContainer.querySelector('input[name="title"]').value,
            description: currentContainer.querySelector('textarea[name="description"]').value
        }
    }
    console.log(update_request);

    postMetadata(document.location, JSON.stringify(update_request));

    // Request successful
    delete currentContainer.dataset.modified;
}

function onModifiedMedia(event) {
    event.currentTarget.dataset.modified = true;
}

document.addEventListener("DOMContentLoaded", function() {
    for(let mediaDiv of document.getElementsByClassName("media")) {
        // Use focusout, because this one will bubble from inner elements
        mediaDiv.addEventListener("focusout", onBlurredMedia);
        mediaDiv.addEventListener("input", onModifiedMedia);
    }
});

/*
  // Get a reference to the body element, and create a new image object
  var body = document.querySelector('body');
  var myImage = new Image();
  // Call the function with the URL we want to load, but then chain the
  // promise then() method on to the end of it. This contains two callbacks
  imgLoad('myLittleVader.jpg').then(function(response) {
    // The first runs when the promise resolves, with the request.response
    // specified within the resolve() method.
    var imageURL = window.URL.createObjectURL(response);
    myImage.src = imageURL;
    body.appendChild(myImage);
    // The second runs when the promise
    // is rejected, and logs the Error specified with the reject() method.
  }, function(Error) {
    console.log(Error);
});
*/